# Deliver app

This is a study purpose project deployed to pythonanywhere.com . You can go to ixa.pythonanywhere.com and test site functionality

## Description

This is a delivery app where you can order foods. You can either register as a customer or restaurant if you have. Users registered as restaurants can add categories, items and take orders. As a customer you can order foods from different restaurants available.

## Note

There are two restaurants and their items. They were created as example. Here is login info for their accounts to test functionality

1. Username - Evos, Email evos@gmail.com, password - 123
2. Username - Oqtepa lavash, oqtepa@gmail.com, password - 123

Examplary customer account login info

Username - ixa, email - ixa@gmail.com, password - 123

You can also create your own restaurant or customer account if you want.

## Technologies Used

1.Python
2.Django
3.Html
4.Bootstrap
